package repositories;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

// Classe intermediaire le compte de l'utilisateur et l'enregistrement Android
public class AccountRepository extends Repository {

	// Constructeur
	public AccountRepository(Context context) {
		super(context);
	}

	// Enregistre le compte dans les SharedPreferences
	public void setAccount(String email) {
		SharedPreferences app = PreferenceManager.getDefaultSharedPreferences(Repository.context);
		Editor prefsEditor = app.edit();
		prefsEditor.putString("EMAIL", email);
		prefsEditor.commit();
	}
	
	// Supprime le compte
	public void unsetAccount() {
		SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(Repository.context);
		Editor prefsEditor = appSharedPrefs.edit();
		prefsEditor.clear();
		prefsEditor.commit();
	}

	// Indique si le compte est configure ou non
	public boolean isAccountConfigured() {
		AccountRepository accRepo = new AccountRepository(Repository.context);
		String email = accRepo.getEmail();

		if (!email.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	// Recupere l'email
	public String getEmail()	{
		SharedPreferences app = PreferenceManager.getDefaultSharedPreferences(Repository.context);
		return app.getString("EMAIL","");
	}

	// Positionne l'email
	public void setEmail(String email)	{
		SharedPreferences app = PreferenceManager.getDefaultSharedPreferences(Repository.context);
		Editor prefsEditor = app.edit();
		prefsEditor.putString("EMAIL",email);
		prefsEditor.commit();	
	}
}

