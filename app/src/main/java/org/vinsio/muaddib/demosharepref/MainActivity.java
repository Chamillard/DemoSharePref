package org.vinsio.muaddib.demosharepref;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import repositories.AccountRepository;

public class MainActivity extends AppCompatActivity {
    AccountRepository accRepo;
    String leEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Creation du compte pour la sauvegarde des informations de l'application
        accRepo = new AccountRepository(getApplicationContext());

        // Verification de l'existence d'un compte
        if (!accRepo.isAccountConfigured()) {
            //Toast.makeText(this,"non configuré ", Toast.LENGTH_LONG).show();

            // Lance la fenetre pour la recherche d'un compte
            LayoutInflater factory = LayoutInflater.from(this);
            final View CompteView = factory.inflate(R.layout.alertdialog_compte, null);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
            final EditText email = (EditText) CompteView.findViewById(R.id.etEmail);
            email.setText("DefaultValue", TextView.BufferType.EDITABLE);

            alertDialogBuilder.setView(CompteView);
            alertDialogBuilder.setIconAttribute(android.R.attr.alertDialogIcon);
            alertDialogBuilder.setTitle(R.string.ad_compte);

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            leEmail = email.getText().toString().trim();
                            accRepo.setEmail(leEmail);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            finish();
                        }
                    })
                    .create();

            AlertDialog ad = alertDialogBuilder.create();
            ad.show();
        }

        String email = accRepo.getEmail();
        setContentView(R.layout.activity_main);
    }
}
